from django.contrib.auth.models import User
from django.db import models


class Article(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Автор')
    preview = models.ImageField(upload_to='blog', verbose_name='Изображение для превью')
    title = models.CharField(max_length=255, verbose_name='Заголовок')
    intro = models.TextField(blank=True, null=True, verbose_name='Вступление')
    body = models.TextField(verbose_name='Содержимое')

    public_after_date = models.DateTimeField(blank=True, null=True, verbose_name='Дата отложенной пуликации')
    public_date = models.DateTimeField(verbose_name='Дата пуликации')

    deleted = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'запись'
        verbose_name_plural = 'записи'
        ordering = ('-public_date',)
