from django.contrib import admin
from django.urls import path, include
from .views import BlogListView, BlogItemView, BlogCreateView, BlogUpdateView, BlogDeleteView

app_name = 'blog'

urlpatterns = [
    path('', BlogListView.as_view(), name='blog'),
    path('create/', BlogCreateView.as_view(), name='create'),
    path('update/<int:pk>/', BlogUpdateView.as_view(), name='update'),
    path('view/<int:pk>/', BlogItemView.as_view(), name='read'),
    path('delete/<int:pk>/', BlogDeleteView.as_view(), name='delete'),

]
