from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DeleteView, UpdateView, CreateView, DetailView

from blog.forms import ArticleForm
from blog.models import Article


class BlogListView(ListView):
    model = Article


class BlogItemView(DetailView):
    model = Article

    @method_decorator(login_required())
    def dispatch(self, *args, **kwargs):
        return super(BlogItemView, self).dispatch(*args, **kwargs)


class BlogCreateView(CreateView):
    model = Article
    form_class = ArticleForm
    success_url = reverse_lazy('blog:blog')

    @method_decorator(login_required())
    def dispatch(self, *args, **kwargs):
        return super(BlogCreateView, self).dispatch(*args, **kwargs)


class BlogUpdateView(UpdateView):
    model = Article
    form_class = ArticleForm
    success_url = reverse_lazy('blog:blog')

    @method_decorator(login_required())
    def dispatch(self, *args, **kwargs):
        return super(BlogUpdateView, self).dispatch(*args, **kwargs)


class BlogDeleteView(DeleteView):
    model = Article
    success_url = reverse_lazy('ordersapp:orders_list')

    @method_decorator(login_required())
    def dispatch(self, *args, **kwargs):
        return super(BlogDeleteView, self).dispatch(*args, **kwargs)


